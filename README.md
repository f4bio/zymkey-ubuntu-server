# docker gitea drone

## TODOs

* ...

## further links

* ...

## prepareations

* [export dotenv to shell](https://gist.github.com/judy2k/7656bfe3b322d669ef75364a46327836)

### (force) use `.env`

```bash
export $(egrep -v '^#' .env | xargs)
```

### create timestamp

```bash
export NOW=$(date +"%Y%m%d-%H%m%S")
```

## example `.env`

```dotenv
...
```

## enable i2c

from: [https://ozzmaker.com/i2c/](https://ozzmaker.com/i2c/)
